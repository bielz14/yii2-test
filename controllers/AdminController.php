<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use app\models\User;
use app\models\Transaction;
use app\models\TransferForm;

class AdminController extends Controller
{    
    public $layout = 'admin';

    public function actionIndex()
    {       
        if (!\Yii::$app->user->can('viewAdminPage')) {
            throw new ForbiddenHttpException('Access denied');
        }

        return $this->render('index');
    }

    public function actionTransaction()
    {       
        if (!\Yii::$app->user->can('viewAdminPage')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $admin = User::findOne(['username' => 'root']);
        $users = User::findAll([
            'status' => User::STATUS_ACTIVE,
        ]);
        $sentTransactions = Transaction::find()->leftJoin('user', '`user`.`id` = `transactions`.`senderId`')->all();
        $transactions = $sentTransactions;

        return $this->render('transaction', 
            [
                'admin' => $admin, 
                'users' => $users, 
                'transaction' => $transaction, 
                'transactions' => $transactions
            ]
        );
    }

    public function actionExecutetransaction()
    {       
        if (!\Yii::$app->user->can('viewAdminPage')) {
            throw new ForbiddenHttpException('Access denied');
        }
        
        $transferBalance = Yii::$app->request->post('transfer_balance');
        $model = User::findOne(['username' => 'root']);
        $currentBalance = $model->balance;
        if (!$transferBalance) {
            $session = Yii::$app->session;
            $session['message'] = 'Transfer amount equals zero';
        } else if ($transferBalance > $currentBalance) {
            $session = Yii::$app->session;
            $session['message'] = 'Your balance is less than the amount transferred';
        } else {
            $model = new TransferForm();
            if ($model->load(Yii::$app->request->post(), '')) {
                $session = Yii::$app->session;
                if (!$model->transfer()) { 
                    $session['message'] = 'No recipient selected';
                } else {
                     $session['message'] = 'Transfer success';
                }
            }
        }

        return $this->goBack(Yii::$app->request->referrer);
    }
}
