<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 */
class m190220_075925_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
            'article' => $this->string()->notNull()->unique(),
            'ownerId' => $this->integer()->notNull(),
            'qty' => $this->integer()->notNull(),
            'cost' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-products-ownerId',
            'products',
            'ownerId'
        );

        $this->addForeignKey(
            'fk-products-ownerId',
            'products',
            'ownerId',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products}}');
    }
}
