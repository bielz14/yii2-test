<?php

use yii\db\Migration;

/**
 * Class m190220_095148_new_resolution
 */
class m190220_095148_new_resolution extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $addingProduct = Yii::$app->authManager->getPermission('addingProduct');
        if (!$addingProduct) {
            $addingProduct = $auth->createPermission('addingProduct');
            $addingProduct->description = 'Добавление продуктов';
            $auth->add($addingProduct);
            $role = $auth->getRole('admin');
            $auth->addChild($role, $addingProduct);
            $role = $auth->getRole('user');
            $auth->addChild($role, $addingProduct);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190220_095148_new_resolution cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190220_095148_new_resolution cannot be reverted.\n";

        return false;
    }
    */
}
