<?php

use yii\db\Migration;
use app\models\User;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190218_135049_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'balance' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->insert('{{%user}}', array(
            'username' => 'root',
            'email'=>'bielz156@mail.ru',
            'password_hash' => password_hash('1111', PASSWORD_DEFAULT),
            'balance' => 1000
        ));

        $userId = \Yii::$app->db->getLastInsertID();
        $auth = Yii::$app->authManager;
        $auth->init();
        $role = $auth->createRole('user');
        $auth->add($role);
        $role = $auth->createRole('admin');
        $auth->add($role);

        $viewAdminPage = $auth->createPermission('viewAdminPage');
        $viewAdminPage->description = 'Просмотр админки';
        $auth->add($viewAdminPage);

        $auth->addChild($role, $viewAdminPage);
        $auth->assign($role, $userId);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
