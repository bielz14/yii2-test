<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transactions}}`.
 */
class m190219_153120_create_transactions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transactions}}', [
            'id' => $this->primaryKey(),
            'senderId' => $this->integer()->notNull(),
            'recipientId' => $this->integer()->notNull(),
            'balance' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-transactions-senderId',
            'transactions',
            'senderId'
        );

        $this->addForeignKey(
            'fk-transactions-senderId',
            'transactions',
            'senderId',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-transactions-recipientId',
            'transactions',
            'recipientId'
        );

        $this->addForeignKey(
            'fk-transactions-recipientId',
            'transactions',
            'recipientId',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transactions}}');
    }
}
