<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Transaction</h1>

    </div>

    <div class="body-content">
    	<div class="container">
	        <div class="row">
	        	<h2>Action</h2>
				<?= Html::beginForm(['executetransaction'], 'post', ['enctype' => 'multipart/form-data', 'style' => 'width: 25%; display: inline-block']) ?>

					<div style="display: inline-block;">
		        		<?= Html::label('Сумма для перечисления:', 'transfer_balance', ['style' => 'color: green']) ?>
					    <?= Html::textInput('transfer_balance', '0', array('class' => 'form-control', 'style' => 'width: 25%; display: inline-block;')) ?>
					    <?= Html::hiddenInput('senderId', Yii::$app->user->id) ?>

					    <div class="form-group" style="display: inline-block;">
					        <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
					    </div>
					   	<div class="self-balance" style="margin-left: 14%; display: inline-block;">
		        			<span style="color: blue">Мой баланс:</span> <?= $admin->balance ?>
		        		</div>
		        	</div>
					
					<div class="error message">
						<?php $session = Yii::$app->session; ?>
		        		<?php if ($session->isActive): ?>
				    		<div class="error message" style="margin-bottom: 1%">
				    			<span style="color: red;"><?= $session['message'] ?></span>
				    		</div>
				    		<?php $session->destroy(); ?>
				    	<?php endif; ?>
		    		</div>

					<div class="users-list" style="margin-left: 10%; display: inline-block;">
						<b><span style="color: gray;">Выберите получателя</span></b>
						<?php if (isset($users)): ?>
							<table class="table table-striped">
							    <thead>
							      <tr>
							        <th>Username</th>
							        <th>Email</th>
							        <th>Balance</th>
							        <th></th>
							      </tr>
							    </thead>
							    <tbody>
							    	<?php foreach ($users as $user): ?>
							    		<?php if ($user->username != 'root'): ?>
								    		<tr>
										        <td><?= $user->username; ?></td>
										        <td><?= $user->email; ?></td>
										        <td><?= $user->balance; ?></td>
										        <td>
										    		<?= Html::radio('recipientId', false, ['value' => $user->id, 'label' => null]); ?>
										    	</td>
										    </tr>
									    <?php endif; ?>
							    	<?php endforeach; ?>
							    </tbody>
							</table>
						<?php else: ?>
							<span style="color: gray">Users missing</span>
						<?php endif; ?>
		    		</div>
	    		<?= Html::endForm() ?>
	    		<h2>History</h2>
	    		<?php if (isset($transactions)): ?>
					<table class="table">
						<thead>
							<tr>
							    <th>Sender</th>
							    <th>Recipient</th>
							    <th>Balance</th>
							    <th>Date</th>
							</tr>
					    </thead>
						<tbody>
							<?php foreach ($transactions as $transaction): ?>
								<?php $senderUsername = $transaction->sender->username; ?>
								<?php $recipientUsername = $transaction->recipient->username; ?>
								<?php if ($senderUsername == 'root'): ?>
									<tr class="danger">
										<td><?= $senderUsername; ?></td>
										<td><?= $recipientUsername; ?></td>
										<td><?= $transaction->balance; ?></td>
										<td><?= date('m-d-Y h:m:s', $transaction->created_at); ?></td>
									</tr>
								<?php else: ?>
									<tr class="success">
										<td><?= $senderUsername; ?></td>
										<td><?= $recipientUsername; ?></td>
										<td><?= $transaction->balance; ?></td>
										<td><?= date('m-d-Y h:m:s', $transaction->created_at); ?></td>
									</tr>
								<?php endif; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php else: ?>
					<span style="margin-left: -14%; color: gray">Transactions missing</span>
				<?php endif; ?>
	        </div>
	     </div>
    </div>
</div>
