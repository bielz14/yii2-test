<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Adding products</h1>

    </div>

    <div class="body-content">
        <div class="container">
            <div class="row">
                <div class="error message">
                    <?php $session = Yii::$app->session; ?>
                    <?php if ($session->isActive): ?>
                        <div class="error message" style="margin-bottom: 1%">
                            <span style="color: red;"><?= $session['message'] ?></span>
                        </div>
                        <?php $session->destroy(); ?>
                    <?php endif; ?>
                </div>
                <?php $form = ActiveForm::begin([
                    'action' => 'addingproduct',
                    'id' => 'login-form',
                    'options' => ['style' => 'width: 25%; display: inline-block'],
                ]) ?>
                    <?= $form->field($product, 'title') ?>
                    <?= $form->field($product, 'article') ?>
                    <?= $form->field($product, 'ownerId')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                    <?= $form->field($product, 'qty') ?>
                    <?= $form->field($product, 'cost') ?>

                    <div class="form-group">
                        <div class="col-lg">
                            <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                <?php ActiveForm::end() ?>
            </div>
         </div>
    </div>
</div>
