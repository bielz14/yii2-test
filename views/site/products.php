<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Products</h1>

    </div>
    <?php if (!Yii::$app->user->isGuest): ?>
        <div style="margin-bottom: 5%">
            <b><span style="color: blue;">My balance: </span></b><?= Yii::$app->user->identity->balance ?>
        </div>
    <?php endif; ?>
    <div class="body-content">
        <div class="col-md-8 products">
            <div class="row">
                                    <div class="error message">
                        <?php $session = Yii::$app->session; ?>
                        <?php if ($session->isActive): ?>
                            <div class="error message" style="margin-bottom: 1%">
                                <span style="color: red;"><?= $session['message'] ?></span>
                            </div>
                            <?php $session->destroy(); ?>
                        <?php endif; ?>
                    </div>
                <?php if (isset($products)): ?>
                    <?php foreach ($products as $product): ?>
                        <?php if ($product->qty): ?>
                            <div class="col-sm-4">
                                <div class="product">
                                  <div class="product-img">
                                     <a href="#"><img src="https://www.accesshq.com/workspace/images/icons/test-your-technology.svg" alt=""></a>
                                  </div>
                                  <p class="product-title">
                                     <p class="product-title"><h3><span class="title"><?= $product->title ?></span></h3></p>
                                  </p>
                                  <p class="product-item"><span class="item">Article: </span><?= $product->article ?></p>
                                  <p class="product-item"><span class="item">Qty: </span><?= $product->qty ?></p>
                                  <p class="product-cost"><span class="item">Price: </span><?= $product->cost ?></p>
                                  <?php if ($product->id != Yii::$app->user->id): ?>
                                    <?= Html::beginForm(['buyproduct'], 'post', ['enctype' => 'multipart/form-data']) ?>
                                        <?= Html::hiddenInput('productId', $product->id) ?>
                                        <?= Html::hiddenInput('customerId', Yii::$app->user->id) ?>
                                        <div class="form-group">
                                            <?= Html::submitButton('Buy', ['class' => 'btn btn-warning']) ?>
                                        </div>
                                    <?= Html::endForm() ?>
                                  <?php endif; ?>
                                </div>

                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <span>Products missing</span>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<style>
    body{
        background: #ececec;
    }
    img {
         height: auto;
         max-width: 100%;
    }
    .content{
        margin-top: 50px;
    }
    .product {
         background: #fff none repeat scroll 0 0;
         border: 1px solid #c0c0c0;
         height: 450px;
         overflow: hidden;
         padding: 25px 15px;
         position: relative;
         text-align: center;
         transition: all 0.5s ease 0s;
         margin-bottom: 20px;
    }
    .product:hover {
        box-shadow: 0 0 16px rgba(0, 0, 0, 0.5);
    }
    .product-img {
        height: 200px;
    }
    .product-title a {
         color: #000;
         font-weight: 500;
         text-transform: uppercase;
    }
    .product-item {
         max-height: 60px;
         overflow: hidden;
    }
    .product-cost {
         bottom: 15px;
         left: 0;
         position: absolute;
         width: 100%;
         color: #d51e08;
         font-size: 18px;
         font-weight: 500;
    }
    span.title {
        color: green;
    }
    span.item {
        color: blue;
    }
</style>