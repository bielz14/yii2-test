<?php
 
namespace app\models;
 
use Yii;
use yii\base\Model;
 
/**
 * Signup form
 */
class AddingproductForm extends Model
{
    public $title;
    public $article;
    public $ownerId;
    public $qty;
    public $cost;

    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'unique', 'targetClass' => '\app\models\Product', 'message' => 'This title has already been taken.'],
            ['title', 'string', 'min' => 2, 'max' => 255],
            ['article', 'required'],
            ['article', 'unique', 'targetClass' => '\app\models\Product', 'message' => 'This article has already been taken.'],
            ['article', 'string', 'min' => 2, 'max' => 255],
            ['ownerId', 'required'],
            ['ownerId', 'integer'],
            ['qty', 'required'],
            ['qty', 'integer'],
            ['qty', 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number'],
            ['cost', 'required'],
            ['cost', 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number']
        ];
    }
 
    public function adding()
    {       
        if ($this->validate()) {
            $title = $this->title;
            $article = $this->article;
            $ownerId = $this->ownerId;
            $qty = $this->qty;
            $cost = $this->cost;

            $product = new Product();
            $product->title = $title;
            $product->article = $article;
            $product->ownerId = $ownerId;
            $product->qty = $qty;
            $product->cost = $cost;
            $product->save();

            return $product;
        }

        return null;
    }
}