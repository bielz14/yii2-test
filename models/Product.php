<?php
 
namespace app\models;
 
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
 
/**
 * Transaction model
 *
 * @property integer $id
 * @property integer $title
 * @property integer $arctile
 * @property integer $ownerId
 * @property integer $qty
 * @property integer $cost
 * @property integer $created_at
 * @property integer $updated_at
 */
class Product extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%products}}';
    }
 
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
 
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'ownerId']);
    }
}