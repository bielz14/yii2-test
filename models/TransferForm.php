<?php
 
namespace app\models;
 
use Yii;
use yii\base\Model;
 
/**
 * Signup form
 */
class TransferForm extends Model
{
    public $senderId;
    public $recipientId;
    public $transfer_balance;

    public function rules()
    {
        return [
            ['senderId', 'required'],
            ['recipientId', 'required'],
            ['transfer_balance', 'required'],
        ];
    }
 
    public function transfer()
    {       
        if ($this->validate()) {
            $senderId = $this->senderId;
            $recipientId = $this->recipientId;
            $transferBalance = $this->transfer_balance;

            $sender = User::findOne($senderId);
            $sender->balance -= $transferBalance;
            $sender->save();
            
            $recipient = User::findOne($recipientId);
            $recipient->balance += $transferBalance;
            $recipient->save();

            $transaction = new Transaction();
            $transaction->senderId = $senderId;
            $transaction->recipientId = $recipientId;
            $transaction->balance = $transferBalance;
            $transaction->save();
            
            return $transaction;
        }

        return null;
    }
}