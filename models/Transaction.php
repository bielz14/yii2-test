<?php
 
namespace app\models;
 
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
 
/**
 * Transaction model
 *
 * @property integer $id
 * @property integer $senderId
 * @property integer $recipientId
 * @property integer $balance
 * @property integer $created_at
 * @property integer $updated_at
 */
class Transaction extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%transactions}}';
    }
 
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
 
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'senderId']);
    }

    public function getRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'recipientId']);
    }
}