<?php
 
namespace app\models;
 
use Yii;
use yii\base\Model;
 
/**
 * Signup form
 */
class BuyproductForm extends Model
{
    public $productId;
    public $customerId;

    public function rules()
    {
        return [
            ['productId', 'required'],
            ['productId', 'integer'],
            ['customerId', 'required'],
            ['customerId', 'integer'],
        ];
    }
 
    public function buy()
    {       
        $session = Yii::$app->session;
        if ($this->validate()) {
            $customerId = $this->customerId;
            $customer = User::findOne($customerId);
            $productId = $this->productId;
            $product = Product::findOne($productId);
            $productCost = $product->cost;
            $customerBalance = $customer->balance;
            if ($customerBalance < $productCost) {
                $session['message'] = 'Your balance is too small';
                return null;
            }
            $customer->balance -= $productCost;
            if ($customer->save()) {
                $product->qty -= 1;
                if ($product->save()) {
                    $admin = User::findOne(['username' => 'root']);
                    $recipientId = $admin->id;
                    $transaction = new Transaction();
                    $transaction->senderId = $customerId;
                    $transaction->recipientId = $recipientId;
                    $transaction->balance = $productCost;
                    $transaction->save();
                }
                return $product;
            }
        }
        
        if (!$this->customerId || $this->customerId == "") {
            $session['message'] = 'Unauthenticated user can make purchases, but he has no balance, so the purchase was not made';
        } else {
            $session['message'] = 'Product not baying';
        }

        return null;
    }
}